const initialState = {
    cards: [{name: "first"},{name: "last"}],
    groups: [{name: "first"},{name: "last"}],
    filters: [{name: "first"},{name: "last"}],
    cardHandler: {
        isFetching: false,
        count: 2
    },
    groupHandler: {
        isFetching: false,
        count: 2
    },
    filterHandler: {
        isFetching: false,
        count: 2
    }
}

export default function (state = initialState, action) {
    return state
}