import './App.css';
import Catalogue from './containers/catalogue/Catalogue';
import BrowserDialog from './containers/browserDialog/BrowserDialog';
import { Container } from '@material-ui/core';

function App() {
  return (<Container maxWidth="md">
    <Catalogue />
    <BrowserDialog />
  </Container>
  );
}

export default App;