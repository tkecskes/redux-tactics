import React from "react";
import { Box } from "@material-ui/core";
import Card from "../../components/cabinetCard/CabinetCard";

export default function CardResults({
    items = []
}) {
    return (<Box p={2}>
        {items.map((item,i) => <Card key={i} {...item} />)}
    </Box>)
}