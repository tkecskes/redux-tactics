import React from "react";
import { Box } from "@material-ui/core";
import Group from "../../components/group/Group";

export default function GroupResults({
    items = []
}) {
    return (<Box p={2}>
        {items.map((item,i) => <Group key={i} {...item} />)}
    </Box>)
}