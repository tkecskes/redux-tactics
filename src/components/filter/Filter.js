import React from "react";
import { Button } from "@material-ui/core";

export default function Filter({
    name = "dunno",
    onChange
}) {
    return (<Button onClick={onChange}>I am a filter named {name}</Button>)
}