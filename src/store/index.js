import { configureStore } from '@reduxjs/toolkit'
import { combineReducers } from "redux";
import catalogueReducer from './catalogueReducer';
import browserDialogReducer from './browserDialogReducer';

const store = configureStore({
  reducer: combineReducers({
    catalogue: catalogueReducer,
    browserDialog: browserDialogReducer,
  })
})
export default store