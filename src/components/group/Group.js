import React from "react";
import { Box } from "@material-ui/core";

export default function Group({
    name = "Fooo no name",
    onClick
}) {
    return (<Box onClick={onClick}>I am a Group named {name}</Box>)
}