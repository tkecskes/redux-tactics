const initialState = {
    cards: [{name: "cfirst"},{name: "clast"}],
    groups: [{name: "cfirst"},{name: "clast"}],
    filters: [{name: "cfirst"},{name: "clast"}],
    cardHandler: {
        isFetching: false,
        count: 2
    },
    groupHandler: {
        isFetching: false,
        count: 2
    },
    filterHandler: {
        isFetching: false,
        count: 2
    }
}

export default function (state = initialState, action) {
    return state
}