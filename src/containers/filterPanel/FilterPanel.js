import React from "react";
import { Box } from "@material-ui/core";
import Filter from "../../components/filter/Filter";
import { useSelector } from "react-redux";

const combineSelectors = (...selectors) =>
   (state) => selectors.reduce((acc, curr) => curr(acc), state)

export default function FilterPanel({
   stateSelector
}) {
   const filters = useSelector(
      combineSelectors(
         stateSelector,
         (state) => state.filters
      )
   )
   return (<Box>
      {filters.map((filter, i) => <Filter key={i} {...filter} />)}
   </Box>)
}