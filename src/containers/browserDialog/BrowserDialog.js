import React from "react";
import { Box, Button, Dialog } from "@material-ui/core";
import FilterPanel from "../filterPanel/FilterPanel";
import CardResults from "../cardResults/CardResults";
import GroupResults from "../groupResults/GroupResults";

const stateRoot = state => state.browserDialog

export default function BrowserDialog() {
    const [open, setOpen] = React.useState(false)
    return (<Box>
        <Button onClick={() => setOpen(true)}>Open dialog</Button>
        <Dialog open={open} onClose={() => setOpen(false)}>
            <FilterPanel stateSelector={stateRoot}/>
            <CardResults />
            <GroupResults />
        </Dialog>
    </Box>)
}