import React from "react";
import { Box } from "@material-ui/core";
import FilterPanel from "../filterPanel/FilterPanel";
import CardResults from "../cardResults/CardResults";
import GroupResults from "../groupResults/GroupResults";

const stateRoot = state => state.catalogue

export default function Catalogue() {
    return (<Box p={1} bgcolor="lightgray">
        <FilterPanel stateSelector={stateRoot} />
        <CardResults />
        <GroupResults />
    </Box>)
}