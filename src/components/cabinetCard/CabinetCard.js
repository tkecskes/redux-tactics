import React from "react";
import { Card, CardContent, CardActions, Button } from "@material-ui/core";

export default function CabinetCard({
    text = "Fooo no text",
    onActionClick
}) {
    return (<Card>
        <CardContent>
            Foooo content
        </CardContent>
        <CardActions>
            <Button onClick={onActionClick}>Fooo</Button>
        </CardActions>
    </Card>)
}